from typing import Any

from paver.easy import call_task, sh, task


def env_do(command: str) -> Any:
    return sh(f'poetry run {command}')


def manage(command: str) -> None:
    env_do(f'python manage.py {command}')


@task
def build():
    call_task('lint')
    call_task('test')


@task
def test():
    env_do('pytest tests')


@task
def lint():
    env_do('flake8 home pets sancapets search')


@task
def mypy():
    env_do('mypy')


@task
def collect_static():
    manage('collectstatic --noinput --clear')


@task
def migrate_db():
    manage('migrate --noinput')


@task
def make_migrations():
    manage('makemigrations')


@task
def run_dev():
    try:
        manage('runserver')
    except KeyboardInterrupt:
        print('Exitting...')


@task
def run_prod():
    env_do('uwsgi')
