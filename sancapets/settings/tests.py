from .base import *  # noqa


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = (
    'django-insecure-9+c6^4k7#%f+btxt7_!w!5yk9#6k&_1d&_^w7$v)l#k%l719&-')


# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ['*']


EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:',
    }
}
