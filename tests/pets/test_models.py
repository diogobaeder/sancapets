import datetime as dt

from django.test import TestCase

from pets.models import AnimalType


class AnimalTypeTest(TestCase):
    def test_contains_basic_fields(self):
        animal_type = AnimalType.objects.create(
            name='Cat',
        )

        self.assertEqual(animal_type.name, 'Cat')
        self.assertIsInstance(animal_type.created_at, dt.datetime)
        self.assertIsInstance(animal_type.updated_at, dt.datetime)
